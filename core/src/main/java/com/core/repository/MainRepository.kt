package com.core.repository

import com.core.api.PagingApi
import com.core.base.BaseObserver
import com.core.base.ResponseAsync
import com.core.dto.GetPhotosRequest
import com.core.dto.Photos
import retrofit2.HttpException
import java.io.IOException


abstract class MainRepository : BaseObserver {

    abstract suspend fun getAllPhotos(
        getPhotosRequest: GetPhotosRequest
    ): ResponseAsync<List<Photos>?>


}

class MainRepositoryImpl(private val pagingApi: PagingApi) : MainRepository() {

    companion object {
        const val NETWORK_PAGE_SIZE = 20
    }


    override suspend fun getAllPhotos(
        getPhotosRequest: GetPhotosRequest
    ): ResponseAsync<List<Photos>?> {
        val tag = "getPhotos"
        return try {
            val res = pagingApi.getPhotos(
                page = getPhotosRequest.page,
                pageSize = getPhotosRequest.pageSize,
            )
            handlerResult(tag, res)
        } catch (exception: IOException) {
            ResponseAsync(mutableListOf(), handleError(tag, exception))
        } catch (exception: HttpException) {
            ResponseAsync(mutableListOf(), handleError(tag, exception))
        }
    }
}
