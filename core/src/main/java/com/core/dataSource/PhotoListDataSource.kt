package com.core.dataSource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.core.dto.*
import com.core.repository.MainRepository
import com.core.repository.MainRepositoryImpl.Companion.NETWORK_PAGE_SIZE


class PhotoListDataSource(
    private val mainRepository: MainRepository
) : PagingSource<Int, Photos>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photos> {
        val pageNumber = params.key ?: 1
        val result =
            mainRepository.getAllPhotos(
                GetPhotosRequest(
                    pageNumber, NETWORK_PAGE_SIZE
                )
            )

        return when (result.networkState.status) {
            Status.FAILED -> {
                LoadResult.Error(Exception(result.networkState.msg))
            }
            else -> {
                val data = result.onSuccess
                val nextPageNumber: Int? = data?.let { res ->
                    if (res.size >= NETWORK_PAGE_SIZE) {
                        pageNumber + 1
                    } else {
                        null
                    }
                } ?: kotlin.run {
                    null
                }
                LoadResult.Page(
                    data = data.orEmpty(),
                    prevKey = if (pageNumber == 1) null else pageNumber - 1,
                    nextKey = nextPageNumber
                )
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Photos>): Int = 1

}