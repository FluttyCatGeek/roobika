package com.core.dto.contacts

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
@Entity(tableName = "contact")
data class ContactDto(
    @PrimaryKey(autoGenerate = true)
    @Keep
    val id: Int,

    @Keep
    var name: String,

    @Keep
    var phoneNumber: String
) : Parcelable
