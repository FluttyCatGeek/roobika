package com.core.dto

import androidx.annotation.Keep

@Keep
class ResultDto<T> {

    @Keep
    var page: Int? = null


    @Keep
    var per_page: Int? = null


    @Keep
    var message: String? = null


    @Keep
    var photos: T? = null
}