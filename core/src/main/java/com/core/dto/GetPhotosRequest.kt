package com.core.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Parcelize
@Keep
data class GetPhotosRequest(
    @Keep var page: Int,
    @Keep var pageSize: Int
) : Parcelable