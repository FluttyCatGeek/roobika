package com.core.db

import androidx.room.*
import com.core.dto.contacts.ContactDto

@Dao
interface ContactDao {

    @Query("delete from contact")
    fun removeContacts()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertContacts(config: ContactDto): Long

    @Transaction
    fun insert(config: ContactDto): Long {
        removeContacts()
        return insertContacts(config)
    }

    @Query("select * from contact")
    fun fetchAll(): List<ContactDto>

    @Query("SELECT * FROM contact WHERE name = :query ")
    fun fetchContactsBySearch(query: String): List<ContactDto>

}