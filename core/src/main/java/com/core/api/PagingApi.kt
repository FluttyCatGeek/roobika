package com.core.api

import com.core.dto.Photos
import com.core.dto.ResultDto
import retrofit2.http.GET
import retrofit2.http.Query


interface PagingApi {

    @GET("curated")
    suspend fun getPhotos(
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int,
    ): ResultDto<List<Photos>>

}