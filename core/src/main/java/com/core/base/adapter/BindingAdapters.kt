package com.core.base.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso

class BindingAdapters {
    companion object {
        /*   @BindingAdapter("app:changeVisibility")
           @JvmStatic
           fun changeVisibility(view: View, value: Boolean) {
               view.visibility = if (value) View.VISIBLE else View.GONE
           }

           @BindingAdapter("appCompatImageUrl")
           @JvmStatic
           fun setImageUrl(imageView: AppCompatImageView, imageUrl: String?) {
               *//*Picasso.get()
                .load("https://ninja.skenap.ir$imageUrl")
                .into(imageView)*//*

            Glide.with(imageView.context).load("https://pnashr.pub/gateway$imageUrl")
                .into(imageView)
        }
*/
        @BindingAdapter("picassoImageUrl")
        @JvmStatic
        fun imageUrl(imageView: ImageView, imageUrl: String?) {
            Picasso.get()
                .load("$imageUrl")
                .resize(50, 50)
                .into(imageView)
        }


        @BindingAdapter("imageUrl")
        @JvmStatic
        fun setImage(imageView: ImageView, imageUrl: String?) {
            Glide.with(imageView.context).load("https://www.pexels.com/photo/$imageUrl")
                .into(imageView)
        }

    }
}

/*
*  val context = imageView.context
            val options: RequestOptions =
                RequestOptions().placeholder(R.drawable.ic_business_donate_v1)
                    .error(R.drawable.ic_launcher_background)
            Glide.with(context)
                .setDefaultRequestOptions(options)
                .load(imageUrl)
                .into(imageView)*/