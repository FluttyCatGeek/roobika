package com.core.base

import com.core.dto.NetworkState

data class ResponseAsync<T>(val onSuccess: T, val networkState: NetworkState)