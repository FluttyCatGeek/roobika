package com.fluttycatgeek.roobika.ui.main.viewModel

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.core.dataSource.PhotoListDataSource
import com.core.dto.Photos
import com.core.repository.MainRepository
import com.core.repository.MainRepositoryImpl.Companion.NETWORK_PAGE_SIZE
import kotlinx.coroutines.flow.Flow

class MainViewModelImpl(var mainRepository: MainRepository) : MainViewModel() {

    override fun requestPhotos(): Flow<PagingData<Photos>> =
        Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                PhotoListDataSource(
                    mainRepository
                )
            }
        ).flow


}