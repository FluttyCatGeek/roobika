package com.fluttycatgeek.roobika.ui.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.core.parent.ParentActivity
import com.fluttycatgeek.roobika.R
import com.fluttycatgeek.roobika.databinding.ActivityMainBinding
import com.fluttycatgeek.roobika.di.DaggerAppComponent
import com.fluttycatgeek.roobika.ui.main.viewModel.MainViewModel
import com.fluttycatgeek.roobika.ui.main.viewModel.MainViewModelFactory
import javax.inject.Inject

class MainActivity : ParentActivity<MainViewModel, ActivityMainBinding>() {

    @Inject
    lateinit var factory: MainViewModelFactory

    private lateinit var navController: NavController

    private var isMainPageDestination = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment? ?: return

        navController = host.navController
        /*     binding.bottomNavigation.setupWithNavController(navController)
            val navBarElevation =
                resources.getDimensionPixelSize(R.dimen.main_navbar_elevation).toFloat()

            ViewCompat.setElevation(binding.bottomNavigation, navBarElevation)

            navController.addOnDestinationChangedListener { _: NavController, destination, _: Bundle? ->
                val id = destination.id
                isMainPageDestination = id == R.id.newsFragment || id == R.id.APODFragment
                setIconOnNavBar(binding.bottomNavigation.menu, id)
                binding.bottomNavigation.visibility =
                    if (isMainPageDestination) View.VISIBLE else View.GONE
            }*/

        setStatusBarColorResource(R.color.colorPrimaryDark)

    }

    /*  private fun setIconOnNavBar(menu: Menu, selectedId: Int) {
          menu.findItem(R.id.APODFragment).icon = ContextCompat.getDrawable(this, R.drawable.ic_apod)
          menu.findItem(R.id.newsFragment).icon = ContextCompat.getDrawable(this, R.drawable.ic_news)
          when (selectedId) {
              R.id.APODFragment -> {
                  menu.findItem(selectedId).icon =
                      ContextCompat.getDrawable(this, R.drawable.ic_apod_active)
              }
              R.id.newsFragment -> {
                  menu.findItem(selectedId).icon =
                      ContextCompat.getDrawable(this, R.drawable.ic_news_active)
              }
          }
      }*/

    override fun getFactory(): ViewModelProvider.Factory = factory

    override fun inject() {
        DaggerAppComponent.builder().app(application).build().inject(this)
    }

    override fun getResourceLayoutId(): Int = R.layout.activity_main

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun showError(tag: String, error: String) {
        super.showError(tag, error)
        showMessage(error)
    }

}