package com.fluttycatgeek.roobika.ui.main.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagingData
import com.core.base.BaseViewModel
import com.core.dto.Photos
import com.core.repository.MainRepository
import kotlinx.coroutines.flow.Flow


abstract class MainViewModel : BaseViewModel() {

    abstract fun requestPhotos(): Flow<PagingData<Photos>>
}


class MainViewModelFactory(
    private var mainRepository: MainRepository,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return MainViewModelImpl(mainRepository) as T
    }

}