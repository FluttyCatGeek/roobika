package com.fluttycatgeek.roobika.ui.splash.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.core.base.BaseViewModel

abstract class SplashViewModel() : BaseViewModel() {

}

class SplashViewModelImpl() : SplashViewModel() {

}

class SplashViewModelFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SplashViewModelImpl() as T
    }
}