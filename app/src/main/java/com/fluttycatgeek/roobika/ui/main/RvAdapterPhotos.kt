package com.fluttycatgeek.roobika.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.core.dto.Photos
import com.fluttycatgeek.roobika.databinding.LayoutItemPhotoHalfBinding
import com.fluttycatgeek.roobika.databinding.LayoutItemPhotosBinding
import java.lang.Exception

class RvAdapterPhotos(private val itemClickCallback: ((Photos) -> Unit)?) :
    PagingDataAdapter<Photos, RecyclerView.ViewHolder>(object :
            DiffUtil.ItemCallback<Photos>() {

            override fun areItemsTheSame(
                oldItem: Photos,
                newItem: Photos
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Photos,
                newItem: Photos
            ): Boolean = oldItem == newItem
        }) {

    private var brickLayoutIsShown: Boolean = false
    private var fakeStarted: Boolean = false

    var fakePosition = -1

    inner class NormalGameViewHolder(var layoutItemPhotosBinding: LayoutItemPhotosBinding) :
        RecyclerView.ViewHolder(layoutItemPhotosBinding.root) {
        fun bind(photos: Photos?) {
            photos?.let {
                layoutItemPhotosBinding.item = it
            }
        }
    }

    inner class EmptyGameViewHolder(var layoutItemPhotoHalfBinding: LayoutItemPhotoHalfBinding) :
        RecyclerView.ViewHolder(layoutItemPhotoHalfBinding.root) {
        fun bind(photos: Photos?) {
            photos?.let {
                layoutItemPhotoHalfBinding.item = it
            }
        }
    }

    override fun getItemCount() =
        try {
            if (getItem(0) == null)
                Int.MAX_VALUE
            else 0
        } catch (e: Exception) {
            0
        }

    override fun getItemViewType(position: Int) = position
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1 && !brickLayoutIsShown) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val normalGameViewBinding =
                LayoutItemPhotoHalfBinding.inflate(layoutInflater, parent, false)
            EmptyGameViewHolder(normalGameViewBinding)
        } else {
            val layoutInflater = LayoutInflater.from(parent.context)
            val normalGameViewBinding =
                LayoutItemPhotosBinding.inflate(layoutInflater, parent, false)
            NormalGameViewHolder(normalGameViewBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val calculateFakePosition = calculateFakePosition(position)
        if (calculateFakePosition == 1 && !brickLayoutIsShown) {
            brickLayoutIsShown = true
            val empty = holder as EmptyGameViewHolder
            empty.bind(getItem(calculateFakePosition))
        } else {
            val game = holder as NormalGameViewHolder
            game.bind(getItem(calculateFakePosition))
        }
    }

    private fun calculateFakePosition(position: Int): Int {
        if (itemCount == 1) {
            if (position == 1) return 1
        }
        return if (position <= itemCount - 1) {
            position
        } else {
            fakeStarted = true
            if (fakePosition == itemCount - 1) {
                fakePosition = -1
            }
            fakePosition++
            fakePosition
        }
    }
}
