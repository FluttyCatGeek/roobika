package com.fluttycatgeek.roobika.ui.main

import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.core.base.PagingLoadStateAdapter
import com.core.parent.ParentSharedFragment
import com.fluttycatgeek.roobika.R
import com.fluttycatgeek.roobika.databinding.FragmentMainBinding
import com.fluttycatgeek.roobika.ui.main.viewModel.MainViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainFragment : ParentSharedFragment<MainViewModel, FragmentMainBinding>() {

    private val adapterPhotos: RvAdapterPhotos by lazy {
        RvAdapterPhotos {
            Toast.makeText(
                requireContext(),
                "image taken by : ${it.photographer}",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun startAnimation(layoutManager: StaggeredGridLayoutManager) {

        val linearSmoothScroller: LinearSmoothScroller =
            object : LinearSmoothScroller(requireContext()) {
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                    return 10000F / displayMetrics.densityDpi
                }
            }

        linearSmoothScroller.targetPosition = Integer.MAX_VALUE
        layoutManager.startSmoothScroll(linearSmoothScroller)
    }
    private var searchJob: Job? = null

    override fun getResourceLayoutId(): Int = R.layout.fragment_main

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        requestList()
    }

    override fun hideProgress(tag: String) {
        super.hideProgress(tag)
        binding.loading = true
    }

    override fun showProgress(tag: String) {
        super.showProgress(tag)
        binding.loading = true
    }

    override fun showError(tag: String, error: String) {
        super.showError(tag, error)
        showMessage(error)
    }

    override fun getSkeletonLayoutId(): View? = binding.photosRv

    private fun initAdapter() {

        with(adapterPhotos) {

            binding.photosRv.apply {
                layoutManager = StaggeredGridLayoutManager(3, LinearLayoutManager.HORIZONTAL)

                binding.photosRv.adapter = this@with
                postponeEnterTransition()
                viewTreeObserver.addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }
                startAnimation(binding.photosRv.layoutManager as StaggeredGridLayoutManager)
            }

            binding.photosRv.adapter = withLoadStateHeaderAndFooter(
                header = PagingLoadStateAdapter(this),
                footer = PagingLoadStateAdapter(this)
            )

            adapterPhotos.addLoadStateListener { loadState ->
                binding.isEmpty =
                    loadState.refresh is LoadState.NotLoading && adapterPhotos.itemCount == 0

                if (loadState.refresh is LoadState.Loading) {
                    skeletonScreen?.show()
                } else {
                    skeletonScreen?.hide()
                }

                val errorState = loadState.source.append as? LoadState.Error
                    ?: loadState.source.prepend as? LoadState.Error
                    ?: loadState.append as? LoadState.Error
                    ?: loadState.prepend as? LoadState.Error

                errorState?.let {
                    skeletonScreen?.hide()
                    it.error.message?.let { msg ->
                        showMessage(msg)
                    }
                }
            }
        }
    }

    private fun requestList() {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.requestPhotos().collectLatest {
                Log.d("TAG", "requestList: $it")
                adapterPhotos.submitData(it)
            }
        }
    }
}
