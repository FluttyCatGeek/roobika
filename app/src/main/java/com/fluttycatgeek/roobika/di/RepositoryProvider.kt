package com.fluttycatgeek.roobika.di

import com.core.api.PagingApi
import com.core.repository.MainRepository
import com.core.repository.MainRepositoryImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit


@Module
class RepositoryProvider {

    /*@Provides
    fun providePagingRepository(retrofit: Retrofit): PagingRepository {
        return PagingRepositoryImpl(retrofit.create(PagingApi::class.java))
    }*/

    @Provides
    fun provideMainRepository(retrofit: Retrofit): MainRepository {
        return MainRepositoryImpl(retrofit.create(PagingApi::class.java))
    }


}