package com.fluttycatgeek.roobika.di

import com.core.repository.MainRepository
import com.fluttycatgeek.roobika.ui.main.viewModel.MainViewModelFactory
import com.fluttycatgeek.roobika.ui.splash.viewModel.SplashViewModelFactory
import dagger.Module
import dagger.Provides


@Module
class ViewModelFactory {

    @Provides
    fun provideSplashViewModelFactory(): SplashViewModelFactory {
        return SplashViewModelFactory()
    }

    @Provides
    fun provideMainViewModelFactory(
        mainRepository: MainRepository,
    ): MainViewModelFactory {
        return MainViewModelFactory(mainRepository)
    }


}